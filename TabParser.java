
import java.util.HashMap;
import java.util.regex.Pattern;

class EParseError extends Exception {
}

class Lexeme {
  public enum TokenType { IDENTIFIER };
  TokenType type;
  String value;
}

class Part {
  // Either-or situation
  String name;
  Track sequence;
}

class TabParser {

  /*
  Lexeme[] tokens_;

  void lex(String input) {
    Pattern label = Pattern.compile("([a-zA-Z_]+):");
    Pattern chord = Pattern.compile("#?[0-9a-x]{6}");
    Pattern quoted = Pattern.compile("\"([^\"])*\"");
    Pattern identifier = Pattern.compile("([a-zA-Z_]+)");
    Pattern tabLine = Pattern.compile("-[EADGBe][|][ |0-9/.hpb]+[|] *\\n");
    Pattern strumLine = Pattern.compile("([|].*)\n");
  }

  Track parse(String input) {
    // Lexical analysis
    tokens_ = lex(input);
    // Parsing
    return parseMusic(tokens);
  }

  Track parseMusic(Lexeme[] tokens) {
    parseMetaInfo(tokens);
    HashMap<String, Part[]> parts;
    while (!tokens.isEmpty()) {
      String label = expect(IDENTIFIER);
      expect(":");
      while (!observe(LABEL)) {
        
        expect(WHITESPACE);
      parts.put(label, parseElement(tokens));
    }
    // Convert names to sequences
    void convertNamesToTracks(String partName) {
      Part[] result = parts.get(part);
      parts.set(
      for (Part part : parts.get(part)) {
        
      if (parts.get(part).name != null) {
        if (parts.get(
    }
      
    for (part: parts.keys())
      convertNameToTrack(part);

    Track result = new Track;
    return result;
  }

  Map parseMetaInfo(Lexeme[] tokens) {
    Map result = new HashMap();
    result["title"] = expect(QUOTED_STRING);
    expect("by");
    result["band"] = expect(QUOTED_STRING);
    return result;
  }

  String parseElement() {
    try { return parseTablature();    } catch (EParseError e) { }
    try { return parseChordDiagram(); } catch (EParseError e) { }
    try { return parseStrumDiagram(); } catch (EParseError e) { }
    try { return parsePartTrack(); } catch (EParseError e) { }
    parseError("element expected");
  }

  String parseTablature(Lexeme[] tokens) {
    final String stringNames = "eBGDAE";
    for (String s : stringNames) {
      expect("-");
      expect(s);
      expect("|");
      // Lexer has to be involved because whitespace is significant.
      //...;
    }
  }

  String parseChordDiagram() {
    String chord = expect(CHORD_DIAGRAM);
    return chord;
  }

  Track parseStrumDiagram() {
    Track result;
    int measures = 0;
    String lastChord = null;
    while (tokens_.top().equals(MEASURE)) {
      Measure measure = expectMeasure();
      if (measure.count() > 0) {
        float duration = 1.0 / measure;
        int count = 0;
        for (String chord : measure) {
          if (chord.equals("/"))
            chord = lastChord;
          else
            lastChord = chord;
          result.addStrum(chord, ++count * duration);
        }
      }
      ++measures;
    }
  }

  private void expect(String token) {
    if (!tokens_.top().equals(token))
      parseError("expected '" + token + "'");
    tokens_.pop();
  }

  private void expect(TokenType tt) {
    if (!tokens_.top().equals(tt))
      parseError("expected particular token type");
    tokens_.pop();
  }
  */
}
