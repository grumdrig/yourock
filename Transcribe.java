// Transcribe.java 
// Print tablature of received MIDI messages

import java.io.IOException;

import javax.sound.midi.Transmitter;
import javax.sound.midi.Receiver;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;

public class Transcribe {

  public static void main(String[] args) 
    throws MidiUnavailableException, InterruptedException {

    boolean useDefaultSynthesizer = false;
    boolean dumpEverything = false;
    int deviceNo = -1;

    for (int i = 0; i < args.length; ++i) {
      String arg = args[i];

      if (arg.equals("-s")) {
        useDefaultSynthesizer = true;
      }
      else if (arg.equals("-d")) {
        dumpEverything = true;
      }
      else {
        try {
          // A device number specified, we guess
          deviceNo = Integer.parseInt(arg);
        } catch (NumberFormatException e) {
          // User's asked for, or otherwise needs, guidance.
          printUsage();
          MidiDecoder.listMidiDevices();
          System.exit(0);
        }
      }
    }

    if (deviceNo == -1) {
      MidiDecoder.listMidiDevices();
      System.exit(0);
    }
    
    TabReceiver rec = new TabReceiver(deviceNo);
    rec.dumpRaw = dumpEverything;
   
    if (useDefaultSynthesizer)
      rec.axe.forwardToDefaultSynth();

    // Wait for enter key, then shut down
    out("Hit [ENTER] when finished");
    try {
      System.in.read();
    } catch (IOException ioe) {
    }
    rec.axe.inputDevice.close();
    try {
      out("Quitting. Hold on one sec...");
      Thread.sleep(1000);
    } catch (InterruptedException e) {
    }
  }
  
  private static void printUsage() {
    out("Transcribe: usage:");
    out("  java Transcribe -h");
    out("    gives help information");
    out("  java Transcribe");
    out("    lists available MIDI devices");
    out("  java Transcribe N");
    out("    read from device number N");
    out("  OPTIONS:");
    out("    -d\tenables debugging output");
    out("    -s\toutput rock and roll using default synthesizer");
  }

  private static void out(String strMessage) {
    System.out.println(strMessage);
  }

  private static void out(Throwable t) {
    t.printStackTrace();
    out(t.toString());
  }
}
