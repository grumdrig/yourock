TODO:

- Compare played to tab to give a numerical score
- Soften scoring of strums, or have optional voicings
- Wait-for-matches mode shouldn't be so disturbing...
- Lead in 1 2 3 anda 4
- Variable-width measures depending on density of notes within (or
  controlled by .vab file)
- Foreshortened staff lines to fit more future stuff onscreen
- Build multipliers when you hit runs (steal from GH)
- Red-yellow-green UV meter for hits and misses (steal from GH)
- Maybe don't debounce
- Soften fret matching for PM notes
- Learn-solo-mode where program tries to figure out where you are and
  smoothly scrolls to follow
- Multiple parts
- Fourier-based speed changes

PROBLEMS WITH THE WHOLE IDEA:

- Hard to watch both guitar & tablature while learning
- MIDI receiver is not super accurate
- Hardware costs money
- Licensing

TO MAYBE NOT DO:

- Parse marker XML file
- Show lyrics

NOTATIONS NOT YET HANDLED:

- Bends/releases properly
- Wide vibrato
- Slide into/out-of upwards/downwards
- Trill
- Harmonics (natural/artificial)
- Tremolo bar dive
- Palm mute (properly)
- Pick slide (display-wise)
- Tremolo picking
- Up-/down-strokes
- Rhythm slashes (for chords)

See http://www.power-tab.net/getarticle.php?id=86
