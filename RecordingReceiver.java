// RecordingReceiver.java
// Contents swiped mostly from DumpReceiver.java of jsresources.org

import java.io.PrintStream;

import javax.sound.midi.MidiMessage;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.MidiUnavailableException;


public class RecordingReceiver implements  Receiver {

  private MidiDecoder axe;
  public Track song;

  public RecordingReceiver(int deviceNo, Track master) 
    throws MidiUnavailableException {
    song = new Track();
    song.master = master;
    axe = new MidiDecoder();
    int[] tuning = ((master != null) ? master : song).tuning;
    if (tuning != null)
      for (int i = 0; i < 6; ++i)
        axe.fretZero[i] += tuning[i];
    axe.connect(deviceNo, this);
  }

  public void close() {
    axe.inputDevice.close();
  }

  public void send(MidiMessage message, long timestamp) {
    synchronized(this) {
      if (message instanceof ShortMessage) {
        // TODO this belongs in midi decoder, i guess
        final ShortMessage m = (ShortMessage) message;
        if (m.getCommand() == 0xb0) {  // spinnin' the volumey knob
          final double cursor = song.playCursor();
          song.tempo = 60 + (int)m.getData2() * 2;
          song.startTime = System.currentTimeMillis() - song.beatsToMs(cursor);
        }
      }
      if (axe.handleMessage(message, timestamp)) {
        song.addLive(axe.lastString,
                     axe.fingered[axe.lastString],
                     axe.velocity[axe.lastString]);
      }
    }
  }

}
