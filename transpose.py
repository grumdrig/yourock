""" Transpose a tab file to make a reasonable first effort at a vab file"""

import sys

block = []

def do(block):
  if len(block) == 2:
    print block[0]
  elif block:
    w = max([len(s) for s in block])
    b = [tuple(s.rjust(w).replace("-",".").replace("|","-")) for s in block]
    trans = apply(zip, b)
    for hline in trans:
      print ''.join(hline)
  del block[:]
    

for line in sys.stdin.read().splitlines():
  line = line.strip()
  if line and ("-" in line):
    block.insert(0,"")
    block.insert(0,line)
  else:
    do(block)
    print line

do(block)
