You Rock
========

Guitar Hero with a real guitar, via MIDI.

Go
--

    $ make
    $ java -cp .:jl1.0.jar YouRock -s message.vab

Status
------

"Half done", it was claimed, once upon a time.

Caveats
-------

Apparently need JDK 1.6 or better.

