
import java.util.LinkedList;
import java.util.List;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Collections;
import java.lang.Thread;
import java.lang.Math;

import javazoom.jl.decoder.JavaLayerException;

class Note /*implements Comparable*/ {
  static final int MAX_VELOCITY = 127;

  int string;
  int fret;
  int velocity;
  double beat;
  double duration = -2.0; // in units of beats. negative means unknown.
  Note match = null;  // used when matched up with another track's notes
  String articulation = null;
  String infortation = null;  // information + notation!

  Note(int string, int fret, int velocity, double beat) {
    this.string = string;
    this.fret = fret;
    this.velocity = velocity;
    this.beat = beat;
  }

  String tab() {
    String result = "";
    for (int i = 0; i < 6; ++i)
      result += String.format("%2s ", (string == i) ? fret : " |");
    return result;
  }
}


class Cue {
  long positionMs; 
  double beat;
  String section;
  
  Cue(long at, int beat, String sectionName) {
    this.positionMs = at;
    this.beat = beat;
    this.section = sectionName;
  }
}


class Track {
  String title;
  String artist;
  
  List<Note> sequence = new LinkedList<Note>();

  double tempo = 120;  // bps

  int[] tuning = null;

  LinkedList<Cue> cues = new LinkedList<Cue>();

  double length = 0.0f;

  int beatsPerBar = 4;  // top half of time signature

  Double offset = null;  // Beat-wise offset within the measure

  Mp3Player mp3 = null;

  Track master = null;  // associated sequence that knows timing better

  // These used only when mp3 is null
  long startTime = 0;  // ms
  long stopTime = 0;   // ms

  static final double DEBOUNCE = 0.1; // beats
  static final int NUM_STRINGS = 6;
  Note[] lastNote = new Note[NUM_STRINGS];
  
  
  Track() {}

  // Composition
  
  Note note(int string, int fret) {
    Note e = new Note(string, fret, Note.MAX_VELOCITY, length);
    int at = sequence.size();
    while (at > 0 && sequence.get(at-1).beat > e.beat) --at;
    sequence.add(at, e);
    lastNote[string] = e;
    return e;
  }

  Note note(int string, int fret, double duration, double advance) {
    final Note result = note(string, fret);
    result.duration = duration;
    this.advance(advance);
    return result;
  }

  Note note(int string, int fret, double advance) {
    return note(string, fret, -2.0, advance);
  }

  void advance(double beats) {
    length += beats;
  }

  Note strum(int[] chord, double duration, double arpeggiation) {
    final int wasCount = sequence.size();
    final double wasLength = length;
    Note result = null;
    for (int i = 0; i < 6; ++i) {
      if (chord[i] >= 0) {
        Note r = note(i, chord[i], arpeggiation/6);
        if (result == null) result = r;
      }
    }
    length = wasLength + duration;
    return result;
  }

  void addCue(int positionMs, String sectionName) {
    int count = (int) Math.round(length);
    cues.add(new Cue(positionMs, (int)Math.round(length), sectionName));
  }

  void addCue(int posMin, int posSec, int posMs, String sectionName) {
    addCue(posMin * 60000 + posSec * 1000 + posMs, sectionName);
  }

  void interpolateCues() {
    // TODO: Shitty way to iterate a list.
    long posB4 = 0;
    double beatB4 = 0;
    double lastMsPerBeat = 60000.0 / tempo;
    for (int i = 0; i < cues.size(); ++i) {
      Cue cue = cues.get(i);
      if (cue.positionMs < 0) {
        // Fix this cue
        for (int a = i + 1; a < cues.size(); ++a) {
          Cue cueAfter = cues.get(a);
          if (cueAfter.positionMs >= 0) {
            lastMsPerBeat = (cueAfter.positionMs - posB4) /
              (cueAfter.beat - beatB4);
            break;
          }
        }
        cue.positionMs = (long)(posB4 + lastMsPerBeat * (cue.beat - beatB4));
      } else {
        posB4 = cue.positionMs;
        beatB4 = cue.beat;
      }
    }  
  }

  // Recording

  /*synchronized*/ void addLive(int string, int fret, int velocity) {
    final double now = playCursor();
    if (lastNote[string] != null && velocity <= 0)
      lastNote[string].duration = playCursor() - lastNote[string].beat;
    if (velocity <= 0)
      return;
    Note e;
    /*
    if (lastNote[string] != null && 
        lastNote[string].beat + DEBOUNCE > now) {
      // Debounce this note
      e = lastNote[string];
      e.fret = fret;
      e.velocity = velocity; // ?
      // Unmatch -- match will be retried below
      if (e.match != null) {
        e.match.match = null;
        e.match = null;
        // TODO: need to check if there's a better, older match, or track multiple matches
      }
      e.duration = -2.0;
      } else*/ {
      e = new Note(string, fret, velocity, playCursor());
      sequence.add(e);
      lastNote[string] = e;
    }

    // We just assume it's a quarter note.
    if (length < e.beat + 1.0) 
      length = e.beat + 1.0;

    // Try to match with an scored note
    // TODO: don't loop over everything every time
    if (master != null)
      findMatch(e);
    
  }

  // Info

  void jumpBack() {
    // Rewind to previous named cue, or the beinning
    long toPosMs = 0;
    for (Cue cue : cues) {
      if (cue.positionMs + 500 < getPosition() &&
          !cue.section.isEmpty())
        toPosMs = cue.positionMs;
      else 
        break;
    }
    jumpTo(toPosMs);
  }

  void jumpForward() {
    // Rewind to next named cue, if any
    for (Cue cue : cues) {
      if (cue.positionMs > getPosition() &&
          !cue.section.isEmpty()) {
        jumpTo(cue.positionMs);
        break;
      }
    }
  }

  void jumpTo(long posMs) {
    if (master != null)
      master.jumpTo(posMs);
    else if (mp3 != null)
      mp3.jumpTo(posMs);
    else if (stopTime > 0)
      stopTime = startTime + posMs;
    else
      startTime = System.currentTimeMillis() - posMs;
  }

  void changeSpeed(int increment) {
    if (mp3 != null)
      mp3.changeSpeed(increment);
    // TODO: else
  }

  String section() {
    final double now = playCursor();
    String result = "";
    for (Cue cue : cues) {
      if (cue.beat <= now)
        result = cue.section;
      else break;
    }
    return result;
  }

  Cue findSection(String name) {
    for (Cue cue : cues)
      if (cue.section.equals(name))
        return cue;
    return null;
  }

  double tempo() {
    final double now = playCursor();
    if (cues.size() < 2)
      return this.tempo;
    for (int i = 1; i < cues.size(); ++i)
      if (cues.get(i).beat > now || i == cues.size() - 1)
        return (60000.0 * (cues.get(i).beat - cues.get(i-1).beat) / 
                (cues.get(i).positionMs - cues.get(i-1).positionMs));
    // We should never get here.
    return this.tempo;
  }
  
  private double msToBeats(long ms) {
    Cue prior = null;
    for (Cue cue: cues) {
      if (cue.positionMs <= ms) {
        // Keep looking for cue just after this point
        prior = cue;
      } else if (prior != null) {
        // Between two cues
        return prior.beat + (cue.beat - prior.beat) * 
          (ms - prior.positionMs) / (cue.positionMs - prior.positionMs);
      } else {
        // Before first cue 
        return cue.beat + (ms - cue.positionMs) * tempo / 60000.0;
      }
    }
    if (prior != null) {
      // After last cue
      // TODO: consider stopped player at end of song
      return prior.beat + (ms - prior.positionMs) * tempo / 60000.0;
    } else {
      // No cues!
      // TODO: lose startup delay since redundant with cues
      return ms * tempo / 60000.0;
    }
  }

  long beatsToMs(double beats) {
    // Only works w/o cues
    return (long) (beats * 60000.0 / tempo);
  }

  synchronized double computeRating() {
    final double atBeat = master.playCursor();
    int hits = 20;  // flattening constant
    int misses = hits;
    Iterator<Note> i = master.sequence.iterator(); 
    while (i.hasNext()) {
      Note ni = i.next();
      if (ni.beat > atBeat) break;
      if (ni.match != null) ++hits;
      else ++misses;
    }
    if (hits + misses == 0) return 0.0;
    return 100.0 * hits / (hits + misses);
      
    /*
    Iterator<Note> iplayed = sequence.iterator();
    Note played = iplayed.hasNext() ? iplayed.next() : null;
    final double windowSize = 1.0;  // # beats to look for matches in each dir.
    double cursor = -1.0;
    LinkedList<Note> playedWindow = new LinkedList<Note>();
    HashMap<Note, Note> picks = new HashMap<Note, Note>();
    for (Iterator<Note> iscored = master.sequence.iterator();
         iscored.hasNext(); ) {
      Note scored = iscored.next();
      // Check for doneness
      if (scored.beat > atBeat) break;
      // Add played notes that have come into the window
      while (played != null && played.beat < scored.beat + windowSize) {
        playedWindow.add(played);
        played = iplayed.hasNext() ? iplayed.next() : null;
      }
      // Drop played notes that have fallen outside the window
      for (Iterator<Note> ipw = playedWindow.iterator(); ipw.hasNext(); ) {
        if (ipw.next().beat < scored.beat - windowSize)
          ipw.remove();
        else
          break;
      }
      // Match one of the played notes to this scored one
      Note best = null;
      double bestRating = 0.0;
      for (Iterator<Note> ipw = playedWindow.iterator(); ipw.hasNext(); ) {
        Note pw = ipw.next();
        if (pw.string == scored.string && pw.fret == scored.fret) {
          // We have a match. Rate it.
          final double rating = rateMatch(pw, scored);
          if (best == null || rating > bestRating) {
            if (picks.containsKey(pw) && rating < rateMatch(pw, picks.get(pw)))
              continue;  // Already spoken for with a better match
            best = pw;
            bestRating = rating;
          }
        }
      }
      // Remember our pick
      if (best != null)
        picks.put(best, scored);
    }
    // TODO: kind of wastefully done
    double result = 0.0;
    for (Iterator<Note> i = picks.keySet().iterator(); i.hasNext(); ) {
      played = i.next();
      result += rateMatch(played, picks.get(played));
    }
    return result;
    */
  }

  private void findMatch(Note played) {
    for (Iterator<Note> i = master.sequence.iterator(); i.hasNext(); ) {
      Note ni = i.next();
      if (ni.beat > played.beat + 1.0) break;  // too far away
      if (ni.beat > played.beat - 1.0 && 
          ni.string == played.string && ni.fret == played.fret) {
        final double rating = rateMatch(played, ni);
        if ((ni.match == null ||
             rating > rateMatch(ni.match, ni)) &&
            (played.match == null ||
             rating > rateMatch(played, played.match))) {
          final Note oldMatch = ni.match;
          if (played.match != null) {
            played.match.match = null;
            played.match = null;
          }
          ni.match = played;
          played.match = ni;
          if (oldMatch != null) {
            oldMatch.match = null;
            findMatch(oldMatch);
          }
        }
      }
    }
  }
  
  private double rateMatch(Note played, Note scored) {
    // Roundaboutly, velocity / dist^2
    double result = played.beat - scored.beat;
    final double epsilon = 0.5;  // Avoid div by 0 && flatten too
    result *= result;
    result += epsilon;
    return /*played.velocity*/ 1.0 / result;
  }

  long getPosition() {
    if (master != null)
      return master.getPosition();
    else if (mp3 != null)
      return mp3.getPosition();
    else if (startTime == 0)
      return 0;
    else if (stopTime != 0) 
      return stopTime - startTime;
    else
      return System.currentTimeMillis() - startTime;
  }

  double playCursor() {
    if (master != null)
      return master.playCursor();
    else 
      return msToBeats(getPosition());
  }

  boolean isPaused() {
    if (mp3 != null)
      return mp3.paused;
    else 
      return startTime == 0 || stopTime > 0;
  }

  // Sequencing

  void play() {
    if (isPaused()) {
      if (mp3 != null) {
        mp3.resume();
      } else if (startTime == 0) {
        startTime = System.currentTimeMillis();
        stopTime = 0;
      } else {
        startTime += System.currentTimeMillis() - stopTime;
        stopTime = 0;
      }
    }
  }

  void pause() {
    if (!isPaused()) {
      if (mp3 != null)
        mp3.pause();
      else 
        stopTime += System.currentTimeMillis();
    }
  } 

  /*
  void playWithIntro(int beats) {
    long startupDelay = 0;
    if (!cues.isEmpty()) {
      startupDelay = cues.get(0).positionMs;
      startupDelay -= (long) (cues.get(0).beat * 60000 / tempo);
    }
    startupDelay += (long) (beatsPerBar * 60000 / tempo);
    startTime = System.currentTimeMillis() + startupDelay;
    try {
      Thread.sleep(startupDelay);
      try {
        mp3.play();
      } catch (JavaLayerException e) {
        System.out.println(e);
      }
    } catch (InterruptedException e) {
      System.out.println(e);
    }
  }
  */

  void playToStdout() {
    double cursor = 0;
    for (Note e : sequence) {
      try { 
        Thread.sleep(Math.round(1000 * (e.beat - cursor) * 60.0 / tempo));
      } catch (InterruptedException ie) {
        break;
      } 
      cursor = e.beat;
      System.out.println(e.tab());
    }
  }
}

