
// Decodes MIDI messages received from a guitar, basically

import javax.sound.midi.MidiMessage;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.SysexMessage;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.Transmitter;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Synthesizer;

class MidiDecoder {
  private static final int[] FRET_ZERO = {40, 45, 50, 55, 59, 64};
  private static final long STRUM_DELAY = 90;

  // Fret fingered on each string
  int fingered[] = { -1, -1, -1, -1, -1, -1 };
  int velocity[] = { 0, 0, 0, 0, 0, 0 };

  // Define the tuning of the guitar
  int[] fretZero = {40, 45, 50, 55, 59, 64};

  // Timestamp of last note on message
  private long lastNoteOn = 0; 
  public int lastStringOn = -1;
  public int lastString = -1;
  boolean newNote = false;

  MidiDevice.Info deviceInfo;
  MidiDevice inputDevice;

  static boolean dumpMessages = false;

  public void dumpMessage(MidiMessage message, long timestamp) {
    System.out.print(String.format("%12d ", timestamp));
    if (message instanceof ShortMessage) {
      ShortMessage m = (ShortMessage)message;
      System.out.print(String.format("SHORT %8d %8d %8d %8d", 
                                       m.getChannel(), m.getCommand(), 
                                       m.getData1(), m.getData2()));
      final int vel = m.getData2();  // s/b 0 for note off
      final int string = 5 - m.getChannel();
      final String fret = (vel <= 0) ? "  x" : 
        String.format("%3d", m.getData1()-fretZero[string]);
      for (int s = 0; s < 6; ++s)
        System.out.print((string == s) ? fret :
                         (velocity[s] > 0) ? "  |" : "  .");
      System.out.println();
    } else if (message instanceof SysexMessage) {
      SysexMessage m = (SysexMessage)message;
      System.out.println("SYSEX");
    } else if (message instanceof MetaMessage) {
      MetaMessage m = (MetaMessage)message;
      System.out.println(String.format("META  %8d", m.getType()));
    } else {
      System.out.println("?UNK? ");
    }
  }

  public boolean handleMessage(MidiMessage message, long timestamp) {
    if (dumpMessages) dumpMessage(message, timestamp);

    if (!(message instanceof ShortMessage)) 
      return false;

    final long systime = System.currentTimeMillis();

    final ShortMessage m = (ShortMessage)(message);

    if (m.getCommand() != 0x90 &&  // note on
        m.getCommand() != 0x80)    // note off
      return false;

    final int vel = m.getData2();  // s/b 0 for note off
    final int string = 5 - m.getChannel();
    final int fret = (vel <= 0) ? -1 : (m.getData1() - fretZero[string]);
    
    newNote = (vel > 0 && systime > lastNoteOn + STRUM_DELAY);
    if (newNote) {
      for (int i = 0; i < 6; ++i) {
        fingered[i] = -1;
        velocity[i] = 0;
      }
    }
    fingered[string] = fret;
    velocity[string] = vel;
    if (vel > 0) {
      lastNoteOn = systime;
      lastStringOn = string;
    }
    lastString = string;
    return true;
  }

  String tablature() {
    String result = "";
    for (int i = 0; i < 6; ++i)
      result += String.format("%2s ", 
                              (fingered[i] < 0) ? " |" : fingered[i]);
    return result;
  }

  void connect(int deviceNo,
               Receiver receiver) throws MidiUnavailableException {
    
    deviceInfo = MidiSystem.getMidiDeviceInfo()[deviceNo];
    inputDevice = MidiSystem.getMidiDevice(deviceInfo);
    inputDevice.open();

    Transmitter  t = inputDevice.getTransmitter();
    t.setReceiver(receiver);

    lastNoteOn = System.currentTimeMillis();

    /*
    try {
      System.in.read();
    } catch (IOException ioe) {
    }
    inputDevice.close();
    System.out.println("Quitting. Hold on one sec...");
    Thread.sleep(1000);
    */
  }

  void forwardToDefaultSynth() throws MidiUnavailableException {
    Synthesizer synth = MidiSystem.getSynthesizer();
    synth.open();
    Receiver r = synth.getReceiver();
    Transmitter  t = inputDevice.getTransmitter();
    t.setReceiver(r);
  }
  
  static void listMidiDevices() throws MidiUnavailableException {
    MidiDevice.Info[] aInfos = MidiSystem.getMidiDeviceInfo();
    for (int i = 0; i < aInfos.length; i++) {
      MidiDevice  device = MidiSystem.getMidiDevice(aInfos[i]);
      System.out.println(i + " " + 
                         (device.getMaxTransmitters() != 0 ? "IN " : "   ") + 
                         (device.getMaxReceivers() != 0 ? "OUT " : "    ") +
                         aInfos[i].getName());
    }
  }
}


