import javazoom.jl.player.*;

import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javazoom.jl.decoder.Bitstream;
import javazoom.jl.decoder.BitstreamException;
import javazoom.jl.decoder.Decoder;
import javazoom.jl.decoder.Header;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.decoder.SampleBuffer;


public class Mp3Player {	  	

  private String filename;
  private Bitstream mp3bits = null;
  private Decoder mpegDecoder = null; 
  private AudioDevice audioOut = null;
  
  private boolean theresMore = true;
  
  private double decodedMsec = 0;  // amount of data decoded this playback
  private long offsetMsec = 0;    // elapsed time before last playback
  
  int slowdown = 1;  // TODO: rationalize

  boolean paused = true;
  Thread  thread = null;
  Thread  nextThread = null;
  
  public Mp3Player(String filename) {
    this.filename = filename;
  }
  
  private Thread playInBackground() {
    thread = new Thread() {
      public void run() { 
        try { 
          play();
          thread = null;
        } catch (JavaLayerException e) { System.out.println(e); }
      }
    };
    thread.start();
    return thread;
  }

  public void pause() {
    //if (thread != null) synchronized(thread) {
      paused = true;
      //} 
  }

  public void resume() {
    //if (thread != null) synchronized(thread) {
      //paused = false;
      //thread.notify();
      if (!paused) return;
      if (thread != null) 
        return;  // Pending resumption anyway
      playInBackground();
      //}
  }
  
  private void play() throws JavaLayerException {
    play(Integer.MAX_VALUE);
  }

  public void changeSpeed(final int increment) {
    if (slowdown + increment >= 1) {
      nextThread = new Thread() {
          public void run() { 
            try { 
              slowdown += increment;
              play();
              thread = null;
            } catch (JavaLayerException e) { 
              System.out.println(e); 
            }
          }
        };
      pause();
    }
  }

  public void jumpTo(final long posMs) {
    if (paused) {
      offsetMsec = posMs;
    } else {
      nextThread = new Thread() {
          public void run() { 
            try { 
              offsetMsec = posMs;
              play();
              thread = null;
            } catch (JavaLayerException e) { 
              System.out.println(e); 
            }
          }
        };
      pause();
    }
  }

  private void open() throws FileNotFoundException, JavaLayerException {
    if (mp3bits == null) {
      final InputStream input = 
        new BufferedInputStream(new FileInputStream(filename));
      mp3bits = new Bitstream(input);
    }
    if (mpegDecoder == null) {
      mpegDecoder = new Decoder();
    }
    if (audioOut == null) {
      decodedMsec = 0;
      if (offsetMsec > 0) skip(offsetMsec);
      audioOut = FactoryRegistry.systemRegistry().createAudioDevice();
      audioOut.open(mpegDecoder);
    }
  }
	
  /**
   * Plays a number of MPEG audio frames. 
   * 
   * @param frames	The number of frames to play. 
   * @return	true if the last frame was played, or false if there are
   *			more frames. 
   */
  private boolean play(int frames) throws JavaLayerException {
    if (!theresMore) return false;

    //int x = 5/0;
    try { open(); } catch(FileNotFoundException e) {
      System.out.println(e);
      return false;
    }

    paused = false;
    while (theresMore && frames-- > 0) {
      if (paused) {
        // Another thread may call pause()
        close();
        break;
      }
      theresMore = decodeFrame();
    }
		
    if (!theresMore) {
      // last frame, ensure all data flushed to the audio device. 
      if (audioOut != null) {				
        audioOut.flush();
        synchronized (this) { close(); }				
      }
    } 
    return theresMore;
  }

  /**
   * Closes this player. Any audio currently playing is stopped
   * immediately. 
   */
  public synchronized void close() {
    if (audioOut != null) { 
      AudioDevice out = audioOut;
      audioOut = null;	
      offsetMsec += out.getPosition() / slowdown;
      // this may fail, so ensure object state is set up before
      // calling this method. 
      out.close();
      try {
        mpegDecoder = null;
        mp3bits.close();
        mp3bits = null;
      } catch (BitstreamException ex) {
        System.out.println(ex);
      }
    }
    if (nextThread != null) {
      thread = nextThread;
      nextThread = null;
      thread.start();
    }
  }
	
  public synchronized boolean isComplete() {
    return !theresMore;
  }
  
  // The position in milliseconds of the current audio sample being
  // played.
  public long getPosition() {
    return ((audioOut == null) ? 0 : (audioOut.getPosition() / slowdown)) + offsetMsec;
  }		

  public boolean isPlaying() {
    return (audioOut != null);
  }

  public boolean skip(final double skipMsec) throws JavaLayerException {
    if (!theresMore) return false;
    if (mpegDecoder == null)
      mpegDecoder = new Decoder();
    final double goal = decodedMsec + skipMsec;
    while (theresMore && decodedMsec < goal) 
      theresMore = decodeFrame();
    offsetMsec = (int)decodedMsec;
    return theresMore;
  }

  // Decodes a single frame. Returns whether there are no more frames
  protected boolean decodeFrame() throws JavaLayerException {		
    try {
      Header h = mp3bits.readFrame();	
      if (h == null)
        return false;
				
      // sample buffer set when decoder constructed
      synchronized (this) {
        if (audioOut != null) {
          final SampleBuffer output = (SampleBuffer)
            mpegDecoder.decodeFrame(h, mp3bits);
          for (int i = 0; i < slowdown; ++i)
            audioOut.write(output.getBuffer(), 0, output.getBufferLength());
        }
      }
      decodedMsec += h.ms_per_frame();

      mp3bits.closeFrame();
    } catch (RuntimeException ex) {
      System.out.println(ex);
      throw new JavaLayerException("Exception decoding audio frame", ex);
    }
    return true;
  }
	
}
