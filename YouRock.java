// YouRock.java

import java.io.IOException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.sound.midi.MidiUnavailableException;


public class YouRock extends JFrame implements WindowListener {
  private static int DEFAULT_FPS = 80;
  private TablaturePanel wp; 

  public static boolean useMidiInput = true;
  public static boolean waitForGuitar = false;
  public static Track song = null;
  
  public YouRock(int period) throws MidiUnavailableException { 
    super("You Ostensibly Rock");
    
    Container c = getContentPane();    // default BorderLayout used
    
    wp = new TablaturePanel(period, song);

    c.add(wp, "Center");
    
    addWindowListener(this);
    pack();
    setResizable(false);
    setVisible(true);
  } 

  // window listener methods

  public void windowActivated(WindowEvent e) { wp.resumeGame(); }
  public void windowDeactivated(WindowEvent e) { wp.pauseGame(); }

  public void windowDeiconified(WindowEvent e) { wp.resumeGame(); }
  public void windowIconified(WindowEvent e) { wp.pauseGame(); }

  public void windowClosing(WindowEvent e) { wp.stopGame(); }
  public void windowClosed(WindowEvent e) {}
  public void windowOpened(WindowEvent e) {}

  // 

  public static void main(String args[]) throws MidiUnavailableException {
    int fps = DEFAULT_FPS;
    long loopStart = 0;
    long loopEnd = -1;
    boolean quiet = false;

    for (int i = 0; i < args.length; ++i) {
      String arg = args[i];
      
      if (arg.equals("-n")) {
        useMidiInput = false;
      } else if (arg.equals("-f")) {
        fps = Integer.parseInt(args[++i]);
      } else if (arg.equals("-d")) {
        MidiDecoder.dumpMessages = true;
      } else if (arg.equals("-w")) {
        waitForGuitar = true;
      } else if (arg.equals("-s")) {
        try {
          song = VabParser.parseFile(args[++i]);
        } catch (IOException e) {
          System.out.println("error reading vab file: " + args[i]);
        } catch (ParseError e) {
          System.out.println("error parsing vab file: " + args[i]);
          System.out.println(e.problem);
          System.out.println(e.line);
          System.out.println(e.lineNo);
          System.exit(-1);
        }
      } else if (arg.equals("-S")) {
        Cue s = song.findSection(args[++i]);
        loopStart = s.positionMs;
      } else if (arg.equals("-S")) {
        Cue s = song.findSection(args[++i]);
        song.mp3.jumpTo(s.positionMs);
      } else if (arg.equals("-L")) {
        Cue s = song.findSection(args[++i]);
        loopEnd = s.positionMs;
      } else if (arg.equals("-q")) {
        quiet = true;
      } else {
        System.out.println("Usage:");
        System.out.println("  -s SONG Load song from specified vab file"); 
        System.out.println("  -S SECT Start playing at named section");
        System.out.println("  -L SECT Loop *until* named section");
        System.out.println("  -n      No guitar MIDI input");
        System.out.println("  -f FPS  Specify graphics frame rate");
        System.out.println("  -d      Enable deubgging dump of MIDI signals");
        System.out.println("  -w      Wait for each note before playing on");
        System.out.println("  -q      Quiet! Don't play the mp3.");
        System.out.println("Devices:");
        MidiDecoder.listMidiDevices();
        System.exit(0);
      }
    }
    
    int period = (int) 1000.0/fps;
    System.out.println("fps: " + fps + "; period: " + period + " ms");

    if (quiet) song.mp3 = null;
    if (loopStart > 0) song.mp3.jumpTo(loopStart);
    TablaturePanel wp = (new YouRock(period)).wp;
    wp.loopStart = loopStart;
    wp.loopEnd = loopEnd;
  }

}


