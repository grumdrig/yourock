import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.awt.Graphics2D;
import java.text.DecimalFormat;
import javax.sound.midi.MidiUnavailableException;


public class TablaturePanel extends JPanel implements Runnable {

  private static final int PWIDTH = 1000;   // size of panel
  private static final int PHEIGHT = 400; 

  private static final int YOFF = 100;
  private static final int YSEP = 30;
  private static final int XBEATSEP = 80;
  private static final int RETICULEX = 200;

  private static final int SECTION_X = 20;
  private static final int SECTION_Y = 350;

  private static final int NO_DELAYS_PER_YIELD = 16;
  /* Number of frames with a delay of 0 ms before the animation thread yields
     to other running threads. */

  private static int MAX_FRAME_SKIPS = 5;   // was 2;
    // no. of frames that can be skipped in any one animation loop
    // i.e the games state is updated but not rendered

  private static StatsAhole stats = new StatsAhole();

  private Thread animator;           // the thread that performs the animation
  private volatile boolean running = false;// used to stop the animation thread

  private int period;                // period between drawing in _ms_

  final private Track song;

  private RecordingReceiver fretwork = null;

  // used at game termination
  private volatile boolean gameOver = false;
  private int score = 0;

  private Font font;
  private FontMetrics metrics;

  private Font smallishfont;
  private Font smallfont;
  private FontMetrics smallmetrics;

  // off screen rendering
  private Graphics2D gbuf; 
  private Image dbImage = null;

  private double rating = 0.0;

  public long loopStart = 0;
  public long loopEnd = -1;

  public TablaturePanel(final int period, final Track song)
    throws MidiUnavailableException {
    this.period = period;
    this.song = song;

    setBackground(Color.white);
    setPreferredSize(new Dimension(PWIDTH, PHEIGHT));

    setFocusable(true);
    requestFocus();    // the JPanel now has focus, so receives key events
    readyForTermination();

    addKeyListener( new KeyAdapter() {
        public void keyPressed(KeyEvent e) { 
          int keyCode = e.getKeyCode();
          if (keyCode == KeyEvent.VK_MINUS) {
            song.changeSpeed(+1);
          } else if (keyCode == KeyEvent.VK_PLUS ||
                     keyCode == KeyEvent.VK_EQUALS) {
            song.changeSpeed(-1);
          } else if (keyCode == KeyEvent.VK_LEFT) {
            song.jumpBack();
          } else if (keyCode == KeyEvent.VK_RIGHT) {
            song.jumpForward();
          } else if (keyCode == KeyEvent.VK_SPACE) {
            if (isPaused())
              resumeGame();
            else
              pauseGame();
          }
        }
      });

    addMouseListener( new MouseAdapter() {
      public void mousePressed(MouseEvent e)
      { testPress(e.getX(), e.getY()); }
    });

    // set up message font
    font = new Font("SansSerif", Font.BOLD, 24);
    metrics = this.getFontMetrics(font);

    smallishfont = new Font("SansSerif", 0, 14);

    smallfont = new Font("SansSerif", 0, 12);
    smallmetrics = this.getFontMetrics(smallfont);

    if (YouRock.useMidiInput) {
      try {
        fretwork = new RecordingReceiver(0, song);
      } catch (MidiUnavailableException e) {
        // I am a horrible, horrible person
        fretwork = new RecordingReceiver(1, song);
      }
    }        
  }

  private void readyForTermination() {
	addKeyListener( new KeyAdapter() {
	// listen for esc, q, end, ctrl-c on the canvas to
	// allow a convenient exit from the full screen configuration
       public void keyPressed(KeyEvent e)
       { int keyCode = e.getKeyCode();
         if ((keyCode == KeyEvent.VK_ESCAPE) || (keyCode == KeyEvent.VK_Q) ||
             (keyCode == KeyEvent.VK_END) ||
             ((keyCode == KeyEvent.VK_C) && e.isControlDown()) ) {
           running = false;
         }
       }
     });
  }

  public void addNotify() {
    // wait for the JPanel to be added to the JFrame before starting
    super.addNotify();   // creates the peer
    startGame();    // start the thread
  }


  private void startGame() {
    // initialise and start the thread 
    if (animator == null || !running) {
      animator = new Thread(this);
	  animator.start();
    }
  }
    

  // ------------- game life cycle methods ------------
  // called by the JFrame's window listener methods


  Track masterTrack() {
    return (song != null) ? song : fretwork.song;
  }

  public boolean isPaused() {
    return masterTrack().isPaused();
  }

  public void resumeGame() {
    masterTrack().play();
  } 

  public void pauseGame() {
    masterTrack().pause();
  } 

  public void stopGame() {
    // called when the JFrame is closing
    running = false;
  }

  // ----------------------------------------------


  private void testPress(int x, int y) {
    // is (x,y) near the head or should an obstacle be added?
    if (!isPaused() && !gameOver) {
      /*
      if (fred.nearHead(x,y)) {   // was mouse press near the head?
        gameOver = true;
        score =  (40 - timeSpentInGame) + (40 - obs.getNumObstacles());    
            // hack together a score
      } else {   // add an obstacle if possible
        if (!fred.touchedAt(x,y))   // was the worm's body untouched?
          obs.add(x,y);
      }
      */
    }
  }  // end of testPress()


  public void run() {
    // The frames of the animation are drawn inside the while loop.
    long beforeTime, afterTime, timeDiff, sleepTime;
    int overSleepTime = 0;
    int noDelays = 0;
    int excess = 0;
    Graphics g;

    stats.start();
    beforeTime = System.currentTimeMillis();

    masterTrack().play();
    /* doesn't work too well; maybe it takes a moment to start the mp3?
    Thread songthread = new Thread() {
        public void run() { 
          try { 
            song.playWithIntro(4); 
          } catch (Exception e) { System.out.println(e); }
        }
      };
    songthread.start();
    */

    running = true;
    
    while(running) {
      gameUpdate(); 
      gameRender();   // render the game to a buffer
      paintScreen();  // draw the buffer on-screen

      afterTime = System.currentTimeMillis();
      timeDiff = afterTime - beforeTime;
      sleepTime = (period - timeDiff) - overSleepTime;  

      if (sleepTime > 0) {   // some time left in this cycle
        try {
          Thread.sleep(sleepTime);  // already in ms
        } catch(InterruptedException ex) { }
        overSleepTime = (int)((System.currentTimeMillis() - afterTime) - sleepTime);
      } else {    // sleepTime <= 0; the frame took longer than the period
        excess -= sleepTime;  // store excess time value
        overSleepTime = 0;

        if (++noDelays >= NO_DELAYS_PER_YIELD) {
          Thread.yield();   // give another thread a chance to run
          noDelays = 0;
        }
      }
      
      beforeTime = System.currentTimeMillis();

      // If frame animation is taking too long, update the game state
      // without rendering it, to get the updates/sec nearer to the
      // required FPS.
      int skips = 0;
      while((excess > period) && (skips < MAX_FRAME_SKIPS)) {
        excess -= period;
        gameUpdate();    // update state but don't render
        skips++;
      }
      
      stats.storeStats(skips, period);
    }

    if (fretwork != null) fretwork.close();

    stats.printStats();
    System.exit(0);   // so window disappears
  } 

  private void gameUpdate() { 
    if (!gameOver) {
      if (song != null && fretwork != null && YouRock.waitForGuitar) {
        final double playCur = song.playCursor();
        boolean missed = false;
        // TODO: slow as usual
        for (Note n : song.sequence) {
          if (n.beat > playCur - 0.5) {
            if (n.beat > playCur - 0.005)
              break;
            if (n.match == null) {
              missed = true;
              break;
            }
          }
        }
        if (!missed && isPaused()) 
          resumeGame();
        if (missed && !isPaused())
          pauseGame();
      }
    }
    if (!isPaused() && !gameOver) {
      if (loopEnd > 0 && song.getPosition() > loopEnd) {
        song.jumpTo(loopStart);
      }
      // Compute a score
      if (stats.frameCount % 5 == 0 && fretwork != null)
        rating = fretwork.song.computeRating();
    }
  } 

  private void gameRender() {
    if (dbImage == null){
      dbImage = createImage(PWIDTH, PHEIGHT);
      if (dbImage == null) {
        System.out.println("dbImage is null");
        return;
      } else {
        gbuf = (Graphics2D) dbImage.getGraphics();
        gbuf.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                              RenderingHints.VALUE_ANTIALIAS_ON);
      }
    }

    // cache this for perf
    final double playCur = masterTrack().playCursor();  

    // clear the background
    gbuf.setColor(Color.white);
    gbuf.fillRect (0, 0, PWIDTH, PHEIGHT);
    
    gbuf.setColor(Color.green);

    gbuf.setFont(smallishfont);
    stats.reportFrameStats(gbuf, playCur);

    gbuf.setColor(Color.blue);
    gbuf.setFont(font);

    // Draw the reticule
    gbuf.setColor(Color.yellow);
    final int reticulex = (song != null) ? RETICULEX : (PWIDTH - RETICULEX);
    gbuf.fillRect(reticulex - metrics.charWidth('8'), YOFF - YSEP, 
                  metrics.charWidth('8') * 2, YSEP * 7);
    
    // Draw the staff lines
    gbuf.setColor(Color.lightGray);
    for (int st = 0; st < 6; ++st) {
      int y = YOFF + st * YSEP;
      gbuf.drawLine(0, y, PWIDTH, y);
    } 

    // Draw measure lines
    final Color veryLightGray = new Color(224, 224, 224);
    for (int beat = 0; ; ++beat) {
      final int x = xoff(playCur) + beat * XBEATSEP;
      if (x >= PWIDTH) break;
      if (x >= 0) {
        gbuf.setColor((beat % masterTrack().beatsPerBar == 0) ? 
                      Color.gray : veryLightGray);
        gbuf.drawLine(x, YOFF, x, YOFF + 5 * YSEP);
      }
    }
    /*
    for (int measure = 0; ; ++measure) {
      final int x = xoff(playCur) + 
        measure * XBEATSEP * masterTrack().beatsPerBar;
      if (x >= PWIDTH) break;
      if (x >= 0)
        gbuf.drawLine(x, YOFF, x, YOFF + 5 * YSEP);
    }
    */

    // Draw the tablature
    if (song != null) {
      gbuf.setColor(Color.blue);
      drawFingering(song, playCur);

      gbuf.setColor(Color.red);
      gbuf.drawString(song.section(), SECTION_X, SECTION_Y);

      gbuf.setFont(smallishfont);
      gbuf.drawString(String.valueOf((int)song.tempo())+" bps", 
                      SECTION_X, SECTION_Y - 50);
      gbuf.drawString("bar " + String.valueOf((int)(playCur / 
                                                 masterTrack().beatsPerBar)), 
                      SECTION_X + 200, SECTION_Y - 50);
      gbuf.drawString(String.format("beat %6.3f", playCur), 
                      SECTION_X + 400, SECTION_Y - 50);
      gbuf.drawString(String.valueOf(song.getPosition()) + "ms", 
                      SECTION_X + 600, SECTION_Y - 50);
      gbuf.setFont(font);
    }

    if (fretwork != null) synchronized(fretwork) {
      gbuf.setColor(Color.black);
      drawFingering(fretwork.song, playCur);
    }

    if (fretwork != null && song != null) {
      // Display rating
      gbuf.drawString(String.format("%8.3f", rating),
                      SECTION_X + 400, SECTION_Y);
      drawRockMeter((int)rating);
      //drawScoreMeter();
    }

    if (gameOver)
      gameOverMessage(gbuf);
  }


  void drawRockMeter(int rating) {
    final int X = 700;
    final int Y = 300;
    final int W = 150;
    final int H = W/4;
    final int U = 3*W/4;
    final int S = (W - U)/2;
    final int M = 2;
    final int P = 1;
    final int[] polyY =  { Y, Y, Y + H, Y + H };
    final int[] rPolyX = { X, X + W/3 - M, X + S + U/3 - M, X + S };
    final int[] yPolyX = { X + W/3 + M, X + 2*W/3 - M, X + S + 2*U/3 - M, 
                           X + S + U/3 - M };
    final int[] gPolyX = { X + 2*W/3+M, X + W+M, X + S + U, X + S +2*U/3 };
    gbuf.setColor(Color.red);
    gbuf.fillPolygon(rPolyX, polyY, 4);
    gbuf.setColor(Color.yellow);
    gbuf.fillPolygon(yPolyX, polyY, 4);
    gbuf.setColor(Color.green);
    gbuf.fillPolygon(gPolyX, polyY, 4);
    gbuf.setColor(Color.black);
    final int lt = X + W * rating / 100;
    final int lb = X + S + U * rating / 100;
    final int[] lpx = { lt-1, lt+1, lb+2, lb-2 };
    final int[] lpy = { Y-1, Y-1, Y+H+1, Y+H+1 };
    gbuf.fillPolygon(lpx, lpy, 4);
  }

  int xoff(double beat) {
    final int reticulex = (song != null) ? RETICULEX : (PWIDTH - RETICULEX);
    return (int)Math.round(reticulex - beat * XBEATSEP);
  }

  void drawFingering(Track song, double beat) {
    final Color wasColor = gbuf.getColor();
    final Color fadeyBlue = new Color(0, 0, 255, 64);
    for (Note e : song.sequence) {
      int x = xoff(beat) + metrics.charWidth('8') / 2 + 
        (int)Math.round(e.beat * XBEATSEP);
      if (x >= -50) {  // assume max char width is no more than 50
        if (x >= PWIDTH) break;  // all done
        int y = YOFF + (5 - e.string) * YSEP + metrics.getAscent() / 3;
        if (e.infortation != null) {
          gbuf.setColor(Color.gray);
          int yy = YOFF - YSEP;
          gbuf.drawString(e.infortation, x, yy);
        }
        final boolean muted = e.duration >= 0.0 && e.duration < 0.1;
        if (e.velocity < e.MAX_VELOCITY) {
          float fv = e.velocity / (float) e.MAX_VELOCITY;
          fv = 1.0f - fv;
          fv *= fv;
          fv = 1.0f - fv;
          final int iv = (int)(255 * fv);
          
          gbuf.setColor((e.match != null) ? 
                        new Color(0, 128, 0, iv) : // green
                        new Color(255, 0, 0, iv)); // red
        } else if (beat > e.beat && (e.match != null)) {
          // A note that was hit fades away
          gbuf.setColor(fadeyBlue);
        } else {
          gbuf.setColor(Color.blue);
        }
        if (e.articulation != null) {
          if (e.articulation.equals("PM") || 
              e.articulation.equals("x") ||
              e.articulation.equals("~")) {
            int yy = YOFF + 6 * YSEP;
            int xx = x;
            if (e.articulation.length() > 1) {
              gbuf.setFont(smallfont);
              xx -= smallmetrics.charWidth('8')/2;
            }
            gbuf.drawString(e.articulation, xx, yy);
            if (e.articulation.length() > 1)
              gbuf.setFont(font);
          } else {
            gbuf.drawString(e.articulation, x-(3*metrics.charWidth('8')/2), y);
          }
        }
        gbuf.drawString(muted ? "x" : String.format("%d", e.fret), x, y);
        if (e.duration >= 0) {
          y = YOFF + (5 - e.string) * YSEP/* - 1*/;
          gbuf.drawLine(x, y, x + (int)(XBEATSEP * e.duration), y);
        }
      }
    }
    gbuf.setColor(wasColor);
  }

  private void gameOverMessage(Graphics g) {
    // center the game-over message in the panel
    String msg = "Game Over. Your Score: " + score;
    int x = (PWIDTH - metrics.stringWidth(msg))/2; 
    int y = (PHEIGHT - metrics.getHeight())/2;
    g.setColor(Color.red);
    g.setFont(font);
    g.drawString(msg, x, y);
  }

  private void paintScreen() {
    // use active rendering to put the buffered image on-screen
    Graphics g;
    try {
      g = this.getGraphics();
      if ((g != null) && (dbImage != null))
        g.drawImage(dbImage, 0, 0, null);
      Toolkit.getDefaultToolkit().sync();  // sync the display on some systems
      g.dispose();
    }
    catch (Exception e) { System.out.println("Graphics error: " + e);  }
  } 

} 


class StatsAhole {
  private static long MAX_STATS_INTERVAL = 1000L;
    // record stats every 1 second (roughly)

  private static int NUM_FPS = 10;
     // number of FPS values stored to get an average


  // used for gathering statistics
  private long statsInterval = 0L;    // in ms
  private long prevStatsTime;   
  private long totalElapsedTime = 0L;
  private long gameStartTime;
  private int timeSpentInGame = 0;    // in seconds

  long frameCount = 0;
  private double fpsStore[];
  private long statsCount = 0;
  private double averageFPS = 0.0;

  private long framesSkipped = 0L;
  private long totalFramesSkipped = 0L;
  private double upsStore[];
  private double averageUPS = 0.0;

  private DecimalFormat df = new DecimalFormat("0.##");  // 2 dp
  private DecimalFormat timedf = new DecimalFormat("0.####");  // 4 dp

  private Font font;

  StatsAhole() {
    // initialise timing elements
    fpsStore = new double[NUM_FPS];
    upsStore = new double[NUM_FPS];
    for (int i=0; i < NUM_FPS; i++) {
      fpsStore[i] = 0.0;
      upsStore[i] = 0.0;
    }
    font = new Font("SansSerif", 0, 14);
  }

  void start() {
    gameStartTime = System.currentTimeMillis();
    prevStatsTime = gameStartTime;
  }

  void storeStats(int skips, int period) {
    framesSkipped += skips;
    /* The statistics:
       - the summed periods for all the iterations in this interval
         (period is the amount of time a single frame iteration should take), 
         the actual elapsed time in this interval, 
         the error between these two numbers;

       - the total frame count, which is the total number of calls to run();

       - the frames skipped in this interval, the total number of frames
         skipped. A frame skip is a game update without a corresponding render;

       - the FPS (frames/sec) and UPS (updates/sec) for this interval, 
         the average FPS & UPS over the last NUM_FPSs intervals.

     The data is collected every MAX_STATS_INTERVAL  (1 sec).
    */
    frameCount++;
    statsInterval += period;

    if (statsInterval >= MAX_STATS_INTERVAL) { 
      long timeNow = System.currentTimeMillis();
      timeSpentInGame = (int) ((timeNow - gameStartTime)/1000L);  // ms -> secs
      long realElapsedTime = timeNow - prevStatsTime;   // time since last stats collection
      totalElapsedTime += realElapsedTime;
      
      double timingError = 
        ((double)(realElapsedTime - statsInterval) / statsInterval) * 100.0;
      
      totalFramesSkipped += framesSkipped;
      
      double actualFPS = 0;     // calculate the latest FPS and UPS
      double actualUPS = 0;
      if (totalElapsedTime > 0) {
        actualFPS = (((double)frameCount / totalElapsedTime) * 1000L);
        actualUPS = (((double)(frameCount + totalFramesSkipped) / totalElapsedTime) 
                     * 1000L);
      }
      
      // store the latest FPS and UPS
      fpsStore[ (int)statsCount%NUM_FPS ] = actualFPS;
      upsStore[ (int)statsCount%NUM_FPS ] = actualUPS;
      statsCount = statsCount+1;
      
      double totalFPS = 0.0;     // total the stored FPSs and UPSs
      double totalUPS = 0.0;
      for (int i=0; i < NUM_FPS; i++) {
        totalFPS += fpsStore[i];
        totalUPS += upsStore[i];
      }
      
      if (statsCount < NUM_FPS) { // obtain the average FPS and UPS
        averageFPS = totalFPS/statsCount;
        averageUPS = totalUPS/statsCount;
      }
      else {
        averageFPS = totalFPS/NUM_FPS;
        averageUPS = totalUPS/NUM_FPS;
      }
      framesSkipped = 0;
      prevStatsTime = timeNow;
      statsInterval = 0L;   // reset
    }
  }  


  void reportFrameStats(Graphics gbuf, double cursor) {
    // Report frame stats
    gbuf.drawString("Average FPS/UPS: " + 
                    df.format(averageFPS) + ", " +
                    df.format(averageUPS) + " ", 20, 25);
  }

  void printStats() {
    System.out.println("Frame Count/Loss: " + 
                       frameCount + " / " + totalFramesSkipped);
    System.out.println("Average FPS: " + df.format(averageFPS));
    System.out.println("Average UPS: " + df.format(averageUPS));
    System.out.println("Time Spent: " + timeSpentInGame + " secs");
    //System.out.println("Boxes used: " + obs.getNumObstacles());
  } 

}


