# Makefile
#

#CLASSPATH = .\;jl1.0.jar 
CLASSPATH = .\:jl1.0.jar 

JFLAGS = -classpath $(CLASSPATH)
JCFLAGS = 
JRFLAGS =
#JAVAC = /c/Program\ Files/Java/jdk1.6.0_02/bin/javac.exe $(JFLAGS) $(JCFLAGS)
JAVAC = javac $(JFLAGS) $(JCFLAGS)
JAVA = java $(JFLAGS) $(JRFLAGS)

JAVASRC = $(shell ls *.java)
CLASSES = $(JAVASRC:.java=.class)

.SUFFIXES : .java .class

.java.class :
	$(JAVAC) $< 

all : compile

compile : $(CLASSES)

doc :
	mkdir -p doc
	javadoc -d doc *.java

clean :
#	rm -f $(CLASSES:.class='$$'*.class) $(CLASSES)
	rm -f *.class
	rm -fr doc

testvab: all
	$(JAVA) VabParser *.vab
