import javazoom.jl.player.*;

import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javazoom.jl.decoder.Bitstream;
import javazoom.jl.decoder.BitstreamException;
import javazoom.jl.decoder.Decoder;
import javazoom.jl.decoder.Header;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.decoder.SampleBuffer;


public class Mp3Shift {	  	

  private static double[][] convert(SampleBuffer buf) {
    double[][] result = 
      new double[buf.getChannelCount()][buf.getBufferLength()/
                                        buf.getChannelCount()];
    int c = 0, s = 0;
    for (int i = 0; i < buf.getBufferLength(); ++i) {
      result[c][s] = buf.getBuffer()[i] / 32767.0;
      if (++c >= buf.getChannelCount()) {
        c = 0;
        ++s;
      }
    }
    return result;
  }

  private static SampleBuffer convert(int freq, 
                                      double[] left, double[] right) {
    SampleBuffer result = new SampleBuffer(freq, 2);
    for (int i = 0; i < left.length; ++i) {
      result.append(0, (short)(left[i] * 32767.0));
      result.append(1, (short)(right[i] * 32767.0));
    }
    return result;
  }

  public static void main(String[] args) 
    throws FileNotFoundException, JavaLayerException, RuntimeException {
    String filename = args[0];
    System.out.println("Rocking '" + filename + "'");
    final InputStream input = 
      new BufferedInputStream(new FileInputStream(filename));
    Bitstream mp3bits = new Bitstream(input);
    Decoder mpegDecoder = new Decoder();
    AudioDevice audioOut = 
      FactoryRegistry.systemRegistry().createAudioDevice();
    audioOut.open(mpegDecoder);

    Transform transformer = new Transform(1024);
    
    while (true) {
      Header h = mp3bits.readFrame();	
      if (h == null)
        break;
      
      final SampleBuffer output = (SampleBuffer)
        mpegDecoder.decodeFrame(h, mp3bits);

      double[][] pre = convert(output);

      double[] post = transformer.shiftPitch(1.0,
                                             4,
                                             output.getSampleFrequency(),
                                             pre[0]);

      SampleBuffer buffer2 = convert(output.getSampleFrequency(),
                                     post, post);

      audioOut.write(buffer2.getBuffer(), 0, buffer2.getBufferLength());

      mp3bits.closeFrame();
    }
		
    audioOut.flush();
    audioOut.close();
    mp3bits.close();
  }
}
