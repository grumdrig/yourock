// TabReceiver.java
// Contents swiped mostly from DumpReceiver.java of jsresources.org

import java.io.PrintStream;

import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.MidiUnavailableException;


public class TabReceiver implements  Receiver {

  public MidiDecoder axe;

  public boolean dumpRaw;

  public TabReceiver(int deviceNo) 
    throws MidiUnavailableException {
    axe = new MidiDecoder();
    axe.connect(deviceNo, this);
  }

  public void close() {
  }

  public void send(MidiMessage message, long timestamp) {
    if (dumpRaw) {
      axe.dumpMessage(message, timestamp);
    } else if (axe.handleMessage(message, timestamp)) {
      if (axe.newNote) System.out.println();
      System.out.print(axe.tablature() + "   @ " + timestamp + "us\r");
    }
  }

}
