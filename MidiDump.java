import java.io.PrintStream;
import java.io.IOException;
import javax.sound.midi.*;


public class MidiDump implements Receiver {

  MidiDevice.Info deviceInfo;
  MidiDevice inputDevice;
  boolean verbose = false;

  public MidiDump(int deviceNo) 
    throws MidiUnavailableException {
    
    deviceInfo = MidiSystem.getMidiDeviceInfo()[deviceNo];
    inputDevice = MidiSystem.getMidiDevice(deviceInfo);
    inputDevice.open();

    Transmitter t = inputDevice.getTransmitter();
    t.setReceiver(this);
  }

  public void send(MidiMessage message, long timestamp) {
    if (!verbose &&
        message.getStatus() == 0xf8) { // MIDI Clock
      return;
    }
    System.out.print(String.format("%12d ", timestamp));
    if (message instanceof SysexMessage) {
      SysexMessage m = (SysexMessage)message;
      System.out.println(String.format("SYSEX [%d]", + m.getData().length));
    } else if (message instanceof ShortMessage) {
      ShortMessage m = (ShortMessage)message;
      System.out.println(String.format("SHORT %1x %1d %3d %3d", 
                                       m.getCommand()>>4, m.getChannel(), 
                                       m.getData1(), m.getData2()));
    } else if (message instanceof MetaMessage) {
      MetaMessage m = (MetaMessage)message;
      System.out.println(String.format("META  %8d", m.getType()));
    } else {
      System.out.println("?UNK? ");
    }
  }

  public void close() {
  }

  public static void main(String[] args) 
    throws MidiUnavailableException, InterruptedException {

 
    int deviceNo = -1;
    if (args.length == 1) {
      try {
        // A device number specified, we guess
        deviceNo = Integer.parseInt(args[0]);
      } catch (NumberFormatException e) {
      }
    }
    if (deviceNo == -1) {
      System.out.println("Usage: java MidiDump DEVICENO\n");
      // List midi devices
      MidiDevice.Info[] aInfos = MidiSystem.getMidiDeviceInfo();
      for (int i = 0; i < aInfos.length; i++) {
        MidiDevice  device = MidiSystem.getMidiDevice(aInfos[i]);
        System.out.println(i + " " + 
                           (device.getMaxTransmitters() != 0 ? "IN " : "   ")+ 
                           (device.getMaxReceivers() != 0 ? "OUT " : "    ") +
                           aInfos[i].getName());
      }
      System.exit(0);
    }

    MidiDump rec = new MidiDump(deviceNo);

    System.out.println("Hit [ENTER] when finished");
    try {
      System.in.read();
    } catch (IOException ioe) {
    }
    rec.inputDevice.close();
    try {
      System.out.println("Quitting. Hold on one sec...");
      Thread.sleep(1000);
    } catch (InterruptedException e) {
    }
  }

}
