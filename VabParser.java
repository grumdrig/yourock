import java.util.LinkedList;
import java.util.ListIterator;
import java.util.HashMap;
import java.io.IOException;
import java.io.FileReader;

class ParseError extends Exception {
  String problem;
  String line;
  int lineNo;

  ParseError(String problem, String line, int lineNo) {
    this.problem = problem;
    this.line = line;
    this.lineNo = lineNo;
  }

  ParseError(String problem) {
    this.problem = problem;
  }
}

class Subtrack {
  Track track = new Track();
  int repeats = 0;
  int indentation = -1;
  boolean substitute = false;
  boolean overlay = false;
  
  void append(Track sub) throws ParseError {
    if (sub.offset != null) 
      checkSync(sub.offset);
    for (Note n: sub.sequence) {
      Note noon = new Note(n.string, n.fret, n.velocity, 
                           n.beat + track.length);
      noon.duration = n.duration;
      noon.articulation = n.articulation;
      noon.infortation = n.infortation;
      int at = track.sequence.size();
      while (at > 0 && track.sequence.get(at-1).beat > noon.beat) --at;
      track.sequence.add(at, noon);
    }
    track.length += sub.length;
  }
  
  void rewind(double beats, boolean erase) {
    // TODO: have to keep the track sorted
    if (erase) {
      ListIterator<Note> i = 
        track.sequence.listIterator(track.sequence.size());
      while (i.hasPrevious() && 
             i.previous().beat >= track.length - beats)
        i.remove();
    }
    track.length -= beats;
  }
  
  static void unindent(LinkedList<Subtrack> substack, int indentation) 
    throws ParseError {
    while (indentation < substack.peek().indentation) {
      Subtrack t = substack.pop();
      if (t.substitute || t.overlay)
        substack.peek().rewind(t.track.length * t.repeats, t.substitute);
          for (int i = 0; i < t.repeats; ++i)
            substack.peek().append(t.track);
    }
  }
  
  void checkSync(double expectedOffset) throws ParseError {
    // A claim that we're at a bar separation
    final double eps = 1.0/1000.0;
    final double beatoff = (eps / 2 + track.length - expectedOffset) % 
      track.beatsPerBar;
    if (beatoff > eps)
      throw new ParseError("beat count at bar is off by " + 
                           beatoff + " beats " + track.length + " " + expectedOffset);
  }
}

class VabParser {

  //private static final Pattern settingLine = 
  //  Pattern.compile("([a-zA-Z_]+)\\b*[=](.*)");
  //private static final Pattern tabLine = 
  //  Pattern.compile("(?(.|[0-9]+|x)\b){6}(.*)");

  public static Track parse(String input) throws ParseError {

    LinkedList<Subtrack> substack = new LinkedList<Subtrack>();
    substack.push(new Subtrack());
    substack.peek().indentation = 0;
    //Track result = substack.peek().track;
    HashMap<String, Track> subtracks = new HashMap<String, Track>();
    HashMap<String, int[]> chords = new HashMap<String, int[]>();
    double duration = 1.0;  // one beat, by default, i.e. a quarter note
    int lineNo = 0;
    for (String line: input.split("\n")) try {
      ++lineNo;
      int lineIndent = 0;
      while (lineIndent < line.length() && line.charAt(lineIndent) == ' ')
        ++lineIndent;
      line = line.trim();
      if (line.length() > 0 && line.charAt(0) != '(') {

        if (substack.peek().indentation < 0) {
          // Indentation not yet determined
          if (lineIndent <= 0)
            throw new ParseError("empty subsequence", line, lineNo);
          else 
            substack.peek().indentation = lineIndent;
        }

        // End completed subsections/repeats
        Subtrack.unindent(substack, lineIndent);

        String infortation = null;
        if (line.contains("\"")) {
          infortation = line.split("\"")[1];
          line = line.split("\"")[0].trim();
        }

        final Track top = substack.peek().track;
        final String[] parts = line.split("\\s");

        if (line.contains("=") && line.replace("=","").trim().length() == 0) {
          // Set the offset for the track
          if (top.offset != null)
            throw new ParseError("offset has already been set");
          top.offset = (top.beatsPerBar - (top.length % top.beatsPerBar))
            % top.beatsPerBar;

        } else if (line.contains("=")) {
          // A setting
          final String[] sides = line.split("=");
          final String name = sides[0].trim();
          final String value = (sides.length > 1) ? sides[1].trim() : "";
          final String value2 = (sides.length > 2) ? sides[2].trim() : "";

          if (name.equals("title")) {
            top.title = value;

          } else if (name.equals("composer")) {
            top.artist = value;

          } else if (name.equals("artist")) {
            top.artist = value;

          } else if (name.equals("album")) {
            // TODO, if we feel like it

          } else if (name.equals("guitar")) {
            // TODO: multiple guitar parts

          } else if (name.equals("tempo")) {
            top.tempo = Double.parseDouble(value.split("\\s+")[0]);

          } else if (name.equals("tuning")) {
            top.tuning = new int[6];
            String[] deltae = value.split("\\s+");
            int delta = 0;
            for (int i = 0; i < 6; ++i) {
              if (i < deltae.length)
                delta = Integer.parseInt(deltae[i]);
              top.tuning[i] = delta;
            }

          } else if (name.equals("time")) {
            String[] signature = value.split("/");
            top.beatsPerBar = Integer.parseInt(signature[0]);
            //TODO top.noteValueOfABeat = 
            //          4.0 / Integer.parseInt(signature[1]);

          } else if (name.equals("audio")) {
            top.mp3 = new Mp3Player(value);

          } else if (name.equals("barre")) {
            // Meaning add a "C" barre notation to output
            // TODO

          } else if (name.equals("cue")) {
            int time;
            if (value.contains(":")) {
              String[] minsec = value.split(":");
              time = (Integer.parseInt(minsec[0]) * 60000 + 
                      (int)(Double.parseDouble(minsec[1]) * 1000));
            } else if (value.length() > 0) {
              time = Integer.parseInt(value);
            } else {
              // To be interpolated later
              time = -1;
            }
            top.addCue(time, value2);

          } else if (name.equals("bar")) {
            // A claim that that's where we are -- check it
            if ((Integer.parseInt(value)-1) * 
                top.beatsPerBar != 
                (int)Math.round(top.length))
              throw new ParseError("incorrect bar claim", line, lineNo);

          } else if (Character.isUpperCase(name.charAt(0))) {
            // Chord definition
            String[] cparts = value.split("\\s");
            int[] chord = new int[6];
            for (int i = 0; i < 6; ++i)
              chord[i] = cparts[i].equals(".") ? -1 : 
                Integer.parseInt(cparts[i]);
            chords.put(name, chord);
          } else {
            throw new ParseError("unrecognised setting: " + name, 
                                 line, lineNo);
          }
          
        } else if (parts[0].equals("substitute") ||
                   parts[0].equals("overlay")) {
          Track s = subtracks.get(parts[1]);
          substack.peek().rewind(s.length, parts[0].equals("substitute"));
          substack.peek().append(s);

        } else if (parts[0].equals("erase") ||
                   parts[0].equals("rewind")) {
          double distance = parseDuration(parts[1]);
          substack.peek().rewind(distance, parts[0].equals("erase"));
          
        } else if (line.endsWith(":")) {
          // Part definition, or repeat
          Subtrack sub = new Subtrack();
          substack.push(sub);
          String[] reps = line.substring(0, line.length() - 1).split("\\s");
          if (reps[0].equals("repeat")) {
            sub.repeats = (reps.length > 1) ? Integer.parseInt(reps[1]) : 2;
          } else if (reps[0].equals("substitute")) {
            sub.substitute = true;
            sub.repeats = 1;
          } else if (reps[0].equals("overlay")) {
            sub.overlay = true;
            sub.repeats = 1;
          } else {
            subtracks.put(reps[0], sub.track);
          }
          
        } else if (Character.isJavaIdentifierStart(line.charAt(0)) && 
                   !parts[0].equals("x")) {
          if (subtracks.get(parts[0]) != null) {
            // Named part substitution
            int reps = 1;
            Track s = subtracks.get(parts[0]);
            for (int p = 1; p < parts.length; ++p) {
              String modifier = parts[p];
              if (modifier.charAt(0) == 'x')
                reps = Integer.parseInt(modifier.substring(1));
              else 
                throw new ParseError("huh?");
            }
            while (reps-- > 0) substack.peek().append(s);
            
          } else if (chords.get(parts[0]) != null) {
            // Named chord
            int[] chord = chords.get(parts[0]);
            int reps = 1;
            boolean arpeggiate = false;
            for (int p = 1; p < parts.length; ++p) {
              if (parts[p].isEmpty()) {
                // Skip nothingness
              } else if (parts[p].charAt(0) == 'x') {
                reps *= Integer.parseInt(parts[p].substring(1));
              } else if (parts[p].contains("/")) {
                duration = parseDuration(parts[p]);
              } else if (parts[p].equals("~~~~~~")) {
                arpeggiate = true;
              } else if (parts[p].equals("fermata")) {
                // TODO
              } else {
                throw new ParseError("garbagey stuff: '" + parts[p] + "'");
              }
            }
            for (int rep = 0; rep < reps; ++rep) {
              final Note n = top.strum(chord, duration, arpeggiate ? 0.5 : 0);
              if (rep == 0 && infortation != null)
                n.infortation = infortation;
            }
          } else {
            throw new ParseError("unrecognized name: " + parts[0]);
          }
        } else if (line.contains("-") && 
                   line.replace("-","").trim().length() == 0) {
          // A claim that we're at a bar separation
          final double expectedOffset = 
            (top.offset == null) ? 0.0 : top.offset;
          substack.peek().checkSync(-expectedOffset);
        } else {
          // Apparently a tablature line
          String[] bits = line.replace(".", " . ").trim().split("\\s+");
          final int numStrings = 6;
          if (bits.length < numStrings)
            throw new ParseError(String.format("expected some tablature (%d)", 
                                               bits.length), line, lineNo);
          LinkedList<Double> durations = new LinkedList<Double>();
          String articulation = null;
          int reps = 1;
          for (int i = numStrings; i < bits.length; ++i) {
            if (bits[i].isEmpty()) {
              // ignore
            } else if (bits[i].equals("h") ||
                       bits[i].equals("p") ||
                       bits[i].equals("t") ||
                       bits[i].equals("s") ||
                       bits[i].equals("b") ||
                       bits[i].equals("r")) {
              articulation = bits[i];
            } else if (bits[i].equals("~") ||
                       bits[i].equals("fermata") ||
                       bits[i].equals("x") ||
                       bits[i].equals("PM")) {
              // TODO Notation for these goes below note (I guess)
              //articulation = bits[i];
              if (bits[i].equals("PM"))
                articulation = bits[i];
            } else if (bits[i].charAt(0) == 'x') {
              reps *= Integer.parseInt(bits[i].substring(1));
            } else if (bits[i].length() > 0) {
              durations.add(parseDuration(bits[i]));
            }
          }
          final double old_duration = duration;
          for (int rep = 0; rep < reps; ++rep) {
            duration = old_duration;
            int durd = 0;
            for (int i = 0; i < numStrings; ++i) {
              if (bits[i].equals(".") ||
                  bits[i].equals("|")) {
                // unvoiced string
              } else if (bits[i].equals("x")) {
                // muted string
                top.note(i, 666, 0.01, 0);
              } else if (bits[i].equals(")")) {
                // extend previous note
                Note prev = top.lastNote[i];
                if (durd < durations.size())
                  duration = durations.get(durd++);
                prev.duration += duration;
              } else {
                // fret number
                if (durd < durations.size())
                  duration = durations.get(durd++);
                int fret;
                if (bits[i].charAt(0) == '/') {
                  // TODO: slide-to articulation
                  bits[i] = bits[i].substring(1);
                }
                try {
                  fret = Integer.parseInt(bits[i]);
                } catch (NumberFormatException e) {
                  throw new ParseError("fret number expected", line, lineNo);
                }
                Note n = top.note(i, fret, duration, 0);
                if (articulation != null)
                  n.articulation = articulation;
                if (infortation != null)
                  n.infortation = infortation;
              }
            }
            if (durd < durations.size())
              duration = durations.get(durd++);
            if (durd < durations.size())
              throw new ParseError("nonsense found at end of line", 
                                   line, lineNo);
            top.advance(duration);
          }
        }
      }
    } catch (ParseError e) {
      e.line = line;
      e.lineNo = lineNo;
      throw e;
    }

    Subtrack.unindent(substack, 0);

    Track result = substack.peek().track;
    result.interpolateCues();
    return result;
  }

  // Returns # beats represented by the fraction
  private static double parseDuration(String s) {
    String[] nd = s.split("/");
    return (4.0 * Integer.parseInt(nd[0]) / Integer.parseInt(nd[1]));
  }

  public static String read(String filename) throws IOException {
    StringBuffer sb = new StringBuffer(1024);
    FileReader reader = new FileReader(filename);
    char[] chars = new char[1024];
    int numRead = 0;
    while( (numRead = reader.read(chars)) > -1)
      sb.append(String.valueOf(chars, 0, numRead));	
    reader.close();
    return sb.toString();
  }

  public static Track parseFile(String filename) 
    throws IOException, ParseError {
    return parse(read(filename));
  }

  public static void main(String args[]) {
    // Test parse of named file
    for (String arg: args) {
      try {
        Track r = parse(read(arg));
        System.out.println("Parsed " + arg);
        /*
        for (Note n: r.sequence) {
          for (int i = 0; i < 6; ++i)
            System.out.print((i == n.string) ? n.fret : "|");
          System.out.println("");
        }
        */
      } catch (IOException e) {
        System.out.println("Error reading " + arg);
      } catch (ParseError e) {
        System.out.println("Error parsing " + arg);
        System.out.println(e.problem);
        System.out.println(String.format("%d: %s", e.lineNo, e.line));
      }
    }
  }
    
}
